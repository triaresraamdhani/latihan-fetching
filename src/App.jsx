import { useState } from "react"
import { Card, Col, Container, Placeholder, Row } from "react-bootstrap"

const App = () => {
  const [state, setState] = useState({
    data: [],
    isLoading: true
  })

  useState(async () => {
    const response = await fetch('https://reqres.in/api/users?page=1&delay=3')
    const result = await response.json()
    setState({
      data: result.data,
      isLoading: false
    })
  }, [])

  return (
    <Container className="mt-3">
      <Row style={{ rowGap: '1rem' }}>
        {(state.isLoading) ? (
          Array.from({ length: 6 }, (element, index) => (
            <Col key={index}>
              <Card style={{ width: '12rem' }}>
                <Placeholder as={Card.Img} variant='top' style={{ width: '100%', height: '10rem' }} />
                <Card.Body>
                  <Placeholder as={Card.Title} animation='glow'>
                    <Placeholder xs={12} />
                  </Placeholder>
                </Card.Body>
              </Card>
            </Col>
          ))
        ) : (
          state.data.map((element, index) => (
            <Col key={index}>
              <Card style={{ width: '12rem' }}>
                <Card.Img variant='top' src={element.avatar} />
                <Card.Body>
                  <Card.Title>
                    {`${element.first_name} ${element.last_name}`}
                  </Card.Title>
                </Card.Body>
              </Card>
            </Col>
          ))
        )}
      </Row>
    </Container>
  )
}

export default App
